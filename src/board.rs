use anyhow::{anyhow, Result};
use comfy_table::{CellAlignment, ColumnConstraint, Table};
use rand::seq::SliceRandom;
use std::convert::TryInto;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};
use std::path::Path;

use crate::spymaster;

pub(crate) struct Board([String; spymaster::GRID_LENGTH]);

impl Board {
    pub(crate) fn from_wordlist<P: AsRef<Path>>(filepath: P) -> Result<Self> {
        let wordlist = Self::read_wordlist(filepath)?;
        let sample = Self::pick_words(&wordlist)?;
        sample
            .try_into()
            .map(Self)
            .map_err(|_| anyhow!("Failed to convert vector to array"))
    }

    fn read_wordlist<P: AsRef<Path>>(filepath: P) -> io::Result<Vec<String>> {
        BufReader::new(File::open(filepath)?).lines().collect()
    }

    fn pick_words(words: &[String]) -> anyhow::Result<Vec<String>> {
        let mut rng = &mut rand::thread_rng();
        let picked: Vec<String> = words
            .choose_multiple(&mut rng, spymaster::GRID_LENGTH)
            .map(String::from)
            .collect();
        Ok(picked)
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut table = Table::new();
        for row in self.0.chunks(5) {
            table.add_row(row);
        }

        let mut max_len = 0_u16;
        for word in &self.0 {
            let len = word.len() as u16;
            if len > max_len {
                max_len = len
            }
        }

        for column in table.column_iter_mut() {
            column.set_constraint(ColumnConstraint::Width(max_len + 2));
            column.set_cell_alignment(CellAlignment::Center);
        }

        writeln!(f, "{}", table)
    }
}
