use comfy_table::Table;
use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};
use std::fmt;

pub(crate) const COLUMNS: usize = 5;
pub(crate) const ROWS: usize = 5;
pub(crate) const GRID_LENGTH: usize = COLUMNS * ROWS;

const BLUE_TEAM_SYMBOL: &'static str = "X";
const RED_TEAM_SYMBOL: &'static str = "O";
const ASSASSIN_SYMBOL: &'static str = "A";
const CIVILIAN_SYMBOL: &'static str = " ";

#[derive(Debug, Clone, Copy)]
pub(crate) enum CellType {
    Red,
    Blue,
    Assassin,
    Civilian,
}

impl fmt::Display for CellType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let repr = match self {
            CellType::Red => RED_TEAM_SYMBOL,
            CellType::Blue => BLUE_TEAM_SYMBOL,
            CellType::Assassin => ASSASSIN_SYMBOL,
            CellType::Civilian => CIVILIAN_SYMBOL,
        };
        write!(f, " {} ", repr)
    }
}

#[derive(Debug)]
pub(crate) enum Team {
    Blue,
    Red,
}

impl fmt::Display for Team {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Team::Blue => write!(f, "Azul [{}]", BLUE_TEAM_SYMBOL),
            Team::Red => write!(f, "Vermelho [{}]", RED_TEAM_SYMBOL),
        }
    }
}
pub(crate) struct SpymasterGrid {
    grid: [CellType; 25],
    lead: Team,
}

impl SpymasterGrid {
    pub(crate) fn new() -> Self {
        use CellType::*;
        let mut rng = thread_rng();
        let blue_leads: bool = rng.gen_bool(0.5);
        let lead: Team;
        let mut grid = [Civilian; 25];
        for cell in &mut grid[0..8] {
            *cell = Blue;
        }
        for cell in &mut grid[8..16] {
            *cell = Red
        }
        grid[16] = Assassin;
        grid[17] = if blue_leads {
            lead = Team::Blue;
            Blue
        } else {
            lead = Team::Red;
            Red
        };
        grid.shuffle(&mut rng);
        SpymasterGrid { grid, lead }
    }
}

impl fmt::Display for SpymasterGrid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut table = Table::new();
        for row in self.grid.chunks(5) {
            table.add_row(row);
        }
        writeln!(f, "{}", table)?;
        writeln!(f, "Equipe Inicial: {}", self.lead)
    }
}
