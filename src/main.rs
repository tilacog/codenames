mod board;
mod spymaster;

fn main() -> anyhow::Result<()> {
    let board = board::Board::from_wordlist("wordlist.txt")?;
    let spymaster_grid = spymaster::SpymasterGrid::new();

    println!("{}", board);
    println!("{}", spymaster_grid);

    Ok(())
}
